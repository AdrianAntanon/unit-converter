package com.example.convertidordeunidades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button buttonUnitConverter;
    private EditText editTextUnitConverter;
    private Spinner spinnerUnitConverter;
    private TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonUnitConverter = findViewById(R.id.buttonUnitConverter);
        editTextUnitConverter = findViewById(R.id.editTextUnitConverter);
        textViewResult = findViewById(R.id.textViewResult);
        spinnerUnitConverter = findViewById(R.id.spinnerUnitConverter);

        editTextUnitConverter.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    unitConversion();
                }
                return false;
            }
        });

        buttonUnitConverter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unitConversion();
            }
        });
    }

    public void unitConversion() {
        String unitToConvert = editTextUnitConverter.getText().toString();
        double INCH = 2.54, CM = 0.393701, YARD = 1.09361, METER = 0.9144, MILE = 0.621371, KM = 1.60934;

        int option = spinnerUnitConverter.getSelectedItemPosition();


        if (unitToConvert.isEmpty()) {
            Toast.makeText(MainActivity.this, "Introduce la unidad de longitud", Toast.LENGTH_LONG).show();
        } else {
            double unit = Double.parseDouble(unitToConvert);
            double result;

            switch (option) {
                case 0:
                    result = unit * INCH;
                    textViewResult.setText("Pulgadas: " + unit + " - Centímetros: " + result);
                    break;
                case 1:
                    result = unit * METER;
                    textViewResult.setText("Yardas: " + unit + " - Metros: " + result);
                    break;
                case 2:
                    result = unit * KM;
                    textViewResult.setText("Millas: " + unit + " - Kilómetros: " + result);
                    break;
                case 3:
                    result = unit * CM;
                    textViewResult.setText("Centímetros: " + unit + " - Pulgadas: " + result);
                    break;
                case 4:
                    result = unit * YARD;
                    textViewResult.setText("Metros: " + unit + " - Yardas: " + result);
                    break;
                case 5:
                    result = unit * MILE;
                    textViewResult.setText("Kilómetros: " + unit + " - Millas: " + result);
                    break;

            }

            textViewResult.setVisibility(View.VISIBLE);
        }


    }
}